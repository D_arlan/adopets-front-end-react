import React, { useState } from 'react'
import api from '../../services/api'

import './style.css'
import logoImg from '../../assets/logo.svg'
import { Link, useHistory } from 'react-router-dom'
import { FiLogIn } from 'react-icons/fi'



export interface Credential{
    
}

export default function LoginPage (){

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const [id, setId] = useState('')
    const [error, setError] = useState(false)
    const history = useHistory()
    
    
    async function handleLogin(e: React.FormEvent) {
        e.preventDefault();
        history.push('/profile')

        const data = {
            email,
            password,
        }

        try {
            setError(false)
            const response = await api.post('login', data)
            console.log(response.data);
            
            localStorage.setItem('userId', response.data.id)
            localStorage.setItem('userName', response.data.username)

            history.push('/profile')
        }
        catch (err) {
            //alert('Falha no login, tente novamente')
            setError(true)
        }
    }

    const phraseError =<p style={{color:'red', textAlign:'center', marginTop:10}}>email or password invalid</p>
    return(
        <div className="logon-container">
            <section className="form">
            <img className="logo" src={logoImg} alt="Logo Be The Hero" />
                <form onSubmit={handleLogin} >
                    <h1>Login</h1>
                    <input
                        placeholder="Email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    <input
                        placeholder="Password"
                        type="password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                    />
                    <button className="button" type="submit">Entrar</button>
                    {error ? phraseError : ''}
                    
                    <Link className="back-link" to="/register"><FiLogIn size={16} color="#E02041" />I want to sign up</Link>
                </form>
            </section>
        </div>
    )
    
}