import React, { useEffect, useState } from 'react'
import api from '../../services/api'

import { Link, useHistory } from 'react-router-dom'
import { FiPower, FiTrash2 } from 'react-icons/fi'
import logoImg from '../../assets/logo.svg'
import './style.css'




export default function Profile() {
    const history = useHistory()

    const [products, setProducts] = useState([])
    
    const userId = localStorage.getItem('userId')
    const userName = localStorage.getItem('userName')


    useEffect(() => {
        api.get('products/', {
            headers: {
                Authorization: userId,
            }
        }).then(response => {
            console.log(response.data.data);
            
            setProducts(response.data.data)
        })
    }, [userId])

    

    function hadleLogout() {
        localStorage.clear()
        history.push('/')
    }

    return (
        <div className="profile-container">
            <header>
                <img src={logoImg} alt="Logo Adopets" />
                <span>Welcome {userName}</span>
                <Link className="button" to="products/new">Register new product</Link>
                <button type="button">
                    <FiPower onClick={hadleLogout} size={18} color="#E02041" />
                </button>
            </header>
            
            <h1>All products</h1>
            
            <ul>
            <li>
                    <strong>Product's name:</strong>
                <p>Ração cão adulto</p>
                    <strong>Description</strong>
                    <p>Ração para cães adultos</p>
                    <strong>Category:</strong>
                    <p>Ração</p>
                    <strong>Price:</strong>
                    <p>99.00</p>
                    <strong>Stock:</strong>
                    <p>12</p>
                    
                </li>
                
                {products.map((product, index) => (
                    <li key={index}>
                    <strong>Product's name:</strong>
                <p>{product.name}</p>
                    <strong>Description</strong>
                    <p>{product.description}</p>
                    <strong>Category:</strong>
                    <p>{product.category}</p>
                    <strong>Price:</strong>
                    <p>{product.price}</p>
                    <strong>Stock:</strong>
                    <p>{product.stock}</p>
                    
                </li>
                ))}
            </ul>
        </div >
    )
}
