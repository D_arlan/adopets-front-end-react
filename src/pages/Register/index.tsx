import React, { useState } from 'react'
import api from '../../services/api'
import './style.css'
import { FiArrowLeft } from 'react-icons/fi'
import { Link, useHistory } from 'react-router-dom'
import logoImg from '../../assets/logo.svg'




export default function Register() {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const history = useHistory()



    async function handleRegister(e: React.FormEvent) {
        e.preventDefault();
        const data = {
            username:name,
            email,
            password,
        }
        try {
            const response = await api.post('register', data);
            history.push('/')
        } catch (err) {
            alert('Registration error, try again.')
        }




    }
    return (
        <div className="register-container">
            <div className="content">
                <section>
                    <img src={logoImg} className="logo" alt="AdoPets Logo" />
                    <h1>Register</h1>
                    <p>Register on our platform</p>
                    <Link className="back-link" to="/"><FiArrowLeft size={16} color="#E02041" />I already have an account</Link>
                </section>
                <form onSubmit={handleRegister}>
                    <input
                        placeholder="Name"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                    <input
                        type="email"
                        placeholder="E-mail"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    <input
                        placeholder="Password"
                        type="password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                    />
                    
                    <button className="button" type="submit">Register</button>
                </form>
            </div>
        </div>
    )
}
