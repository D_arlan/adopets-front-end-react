import React, { useState } from 'react'
import api from '../../services/api'

import './style.css'
import { FiArrowLeft } from 'react-icons/fi'
import { Link, useHistory } from 'react-router-dom'
import logoImg from '../../assets/logo.svg'

export default function NewProduct() {
    const history = useHistory()

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [stock, setStock] = useState('')
    const [category, setCategory] = useState('')

    const userId = localStorage.getItem('userId')

    async function hadleNewProducts(e: React.FormEvent) {
        e.preventDefault();
        const data = {
            name,
            user_id:userId,
            description,
            price:10,
            stock:11,
            category
        }


        console.log(data);
        
        try {
             await api.post('products/1', data)
            
            history.push('/profile')
        } catch (err) {
            alert('Error registering product try again!')
        }
    }


    return (
        <div className="new-incident-container">
            <div className="content">
                <section>
                    <img className="logo" src={logoImg} alt="Logo Be The Hero" />
                    <h1>Register a new product</h1>
                    <p>Describe your product in detail</p>
                    <Link className="back-link" to="/profile"><FiArrowLeft size={16} color="#E02041" />Back to home</Link>
                </section>
                <form onSubmit={hadleNewProducts}>
                    <input
                        placeholder="Name"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                    <input
                        placeholder="Category"
                        value={category}
                        onChange={e => setCategory(e.target.value)}
                    />

                    <input
                        placeholder="Stock"
                        value={stock}
                        onChange={e => setStock(e.target.value)}
                    />
                    
                    <textarea
                        placeholder="Description"
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                    />

                    <input
                        placeholder="Price"
                        value={price}
                        onChange={e => setPrice(e.target.value)}
                    />

                    <button className="button" type="submit">Register</button>
                </form>
            </div>
        </div>
    )
}
