import axios from 'axios'
const api = axios.create({
    baseURL: "https://api-adopets-app.herokuapp.com"
})
export default api;